import React, {useState} from 'react'
import TodoList from "./Todo/TodoList";
import Context from './context'
import AddTodo from "./Todo/AddTodo";

function App() {

    // let todosArr = [
    //     {id: 1, title: 'Купить хлеб', completed: false},
    //     {id: 2, title: 'Купить молоко', completed: false},
    //     {id: 3, title: 'Купить масло', completed: false},
    // ]

    const [todos, setTodos] = useState([
        {id: 1, title: 'Купить хлеб', completed: false},
        {id: 2, title: 'Купить молоко', completed: false},
        {id: 3, title: 'Купить масло', completed: false},
    ])


    function toggleTodo(id) {
        setTodos( todos.map( todo => {
            if(todo.id === id) {
                todo.completed = !todo.completed
            }
            return todo
        }))
    }

    function removeTodo(id) {
        setTodos( todos.filter(todo => {
            return todo.id !== id
        }))
    }

    function addTodo(title) {
        setTodos(todos.concat([{
            title: title,
            id: Date.now(),
            completed: false
        }]))
    }


    return (
        <Context.Provider value={{removeTodo: removeTodo}}>
            <div className='wrapper'>
                <h1>To-Do</h1>
                <p className='smallText'>React App</p>

                

                { todos.length ? <TodoList todos={todos} toToggle={toggleTodo}></TodoList> : <p>No Todos!</p>}

                <AddTodo onCreate={addTodo}></AddTodo>

            </div>
        </Context.Provider>
    );
}

export default App;
