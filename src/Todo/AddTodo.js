import React, {useState} from 'react'
import classes from './AddTodo.module.css'
import PropTypes from 'prop-types'

function AddTodo({onCreate}) {
    const [value, setValue] = useState('')

    function submitHandler(event) {
        event.preventDefault();

        if(value.trim().length > 0) {
            onCreate(value);
            setValue('')
        }
    }

    return(
        <form onSubmit={submitHandler}>
            <input className={classes.add} value={value} onChange={ event => setValue(event.target.value)}/>
            <button className={classes.btn} type='submit'>Add todo</button>
        </form>
    )
}

AddTodo.propTypes = {
    onCreate: PropTypes.func.isRequired
}

export default AddTodo;