import React from "react";
import PropTypes from 'prop-types'
import TodoItem from "./TodoItem";

const styles = {
    ul: {
        listStyle: 'none',
        padding: '0',
        margin: '0'
    }
}
function TodoList(props) {
    // console.log(props)
    return(
      <ul style={styles.ul}>
          { props.todos.map( (todo, index) => {
              // console.log(index)
              return <TodoItem todoItem={todo} key={todo.id} index={index} onChange={props.toToggle}></TodoItem>
          //    для каждого итерируемого объекта нужно прописать уникальный ключ
          }) }
      </ul>
    );
}

// проверка на входящий тип
TodoList.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.object).isRequired,
    toToggle: PropTypes.func.isRequired
}


export default TodoList;