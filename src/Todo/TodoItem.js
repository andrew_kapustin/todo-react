import React, {useContext} from "react";
import PropTypes from 'prop-types'
import classes from './TodoItem.module.css'
import Context from '../context'

function TodoItem(props) {
    const {removeTodo} = useContext(Context)
    const addClass = [];

    if(props.todoItem.completed) {
        addClass.push('done');
    }

    return(
        <li className={classes.item}>
            <span className={addClass.join(' ')}>
                <input
                    checked={props.todoItem.completed}
                    type='checkbox'
                    onChange={ ()=> {
                        props.onChange(props.todoItem.id)
                    }}
                />
                <strong>{props.index+1}</strong>
                &#41;
                &nbsp;
                <span className={classes.title}>{props.todoItem.title}</span>
            </span>
                    
            <button
                className={classes.close}
                onClick={ () => { removeTodo(props.todoItem.id)}}
            >
                &times;</button>
        </li>
    );
}

TodoItem.propTypes = {
    todoItem: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
}

export default TodoItem;